<?php

namespace App\Cignium;

/**
 * 
 */
class InsuranceLookup {
    
    /**
     * 
     * @var array
     */
    protected $records = [];
    
    /**
     * 
     * @var array
     */
    protected $allowedLookupFields = ['zipcode', 'year', 'type'];
    
    /**
     *
     * @var array 
     */
    protected $allowedSortFields = ['name', 'rating'];
    

    /**
     * File name of 
     * 
     * @param string $dataFile
     */
    public function __construct($dataFile) {
        $rawRecords = json_decode(file_get_contents($dataFile), true);
        $this->records = array_map(array($this, 'formatEntry'), $rawRecords);
    }
    
    /**
     * 
     * @param array $criteria
     * @param string $orderBy
     * @param string $orderDir
     * @return array
     */
    public function search(array $criteria, $orderBy='name', $orderDir='asc') {
        if (key_exists('type', $criteria) && $criteria['type'] == 'all') {
            unset($criteria['type']);
        }
        
        $ret = $this->filter($criteria);
        $ordered = $this->sortRecords($ret, $orderBy, $orderDir);
        return $ordered;
    }
    
    /**
     * Return a filtered set of records
     * 
     * Note: This filter could have been done with array_filter func, but for 
     *  some reason, it was always returning an empty array.
     * 
     * @param array $criteria
     * @return array
     */
    protected function filter(array $criteria) {
        $filtered = [];
        foreach ($this->records as $record){
            if ($this->matchesCriteria($criteria, $record)) {
                $filtered[] = $record;
            }
        }
        return $filtered;
    }
    
    /**
     * Returns true if the given record matches the criteria
     * 
     * @param array $criteria
     * @param array $record
     * @return boolean
     */
    protected function matchesCriteria(array $criteria, array $record) {
        foreach ($criteria as $field => $value) {
            if (!in_array($field, $this->allowedLookupFields)){
                continue;
            }
            
            if ($record[$field] != $value) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Sort records by the given field name
     * 
     * @param array $records
     * @param string $field
     * @param string $dir
     * @return array
     */
    protected function sortRecords(array $records, $field, $dir='asc') {
        if (!in_array($field, $this->allowedSortFields)) {
            return $records;
        }
        
        uasort($records, function ($a, $b) use ($field, $dir) {
            if ($a[$field] == $b[$field]) {
                return 0;
            }
            
            if ($dir == 'asc') {
                return ($a[$field] < $b[$field]) ? -1 : 1;
            }
            
            return ($a[$field] > $b[$field]) ? -1 : 1;
            
        });
        
        # removing indexes added by uasort
        $sorted = [];
        foreach ($records as $rec) {
            $sorted[] = $rec;
        }

        return $sorted;
    }
    
    /**
     * Changes raw-format to the API's required format
     * 
     * @param array $entry
     * @return array
     */
    protected function formatEntry(array $entry) {
        $rec = [
            "code" => $entry['PlanCode'],
            "type" => $entry['planType'],
            "year" => $entry['planYear'],
            "rating" => $entry['planRating'],
            "name" => $entry['PlanName'],
            "carrier" => $entry['carrier'],
            "zipcode"  => $entry['availableIn'],
            "drugCoverage" => boolval($entry['drugCoverage']),
            "monthlyDrugPremium" => $entry['MonthlyDrugPremium'],
            "monthlyCost" => $entry['MonthlyCost'],
            "tiers" => []
        ];
        
        foreach ($entry['PlanTiers'][0]['TierInformation'] as $tier) {
            $rec['tiers'][] = [
                    "number" => $tier['TierNumber'],
                    "description" => $tier['Description'],
                    "amount" => $tier['Amount']
                ];
        }
        
        return $rec;
    }
    
}
