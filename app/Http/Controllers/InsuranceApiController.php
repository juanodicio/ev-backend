<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cignium\InsuranceLookup;

class InsuranceApiController extends Controller
{
    
    public function index(Request $request) {
        $datafile = resource_path('data/data.json');
        $lookup = new InsuranceLookup($datafile);
        
        $year = $request->query('year');
        $zipcode = $request->query('zipcode');
        $planType = $request->query('type');
        $orderBy = $request->query('orderBy', 'name');
        $orderDir = $request->query('orderDir', 'asc');
        
        $records = $lookup->search(
            ['year' => $year, 'zipcode' => $zipcode, 'type' => $planType],
            $orderBy, $orderDir
        );
        
        $response = [
            "data" => [
                "plans" => $records,
                "query" => [
                    "year" => $year,
                    "zipcode" => $zipcode,
                    "plans" => $planType,
                    "orderBy" => $orderBy,
                    "orderDir" => $orderDir
                ]
            ]
        ];
        
        return response()->json($response);
    }
}
