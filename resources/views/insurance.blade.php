@extends('layouts.app')


@section('content')
    
<div class="row form-sec">
    
    <form class="form-horizontal" id="searchForm" method="get" action="">

      <div class="form-group">
        <label for="year" class="col-sm-2 control-label">Year</label>
        <div class="col-sm-3">
            <input type="number" required value="" name="year" maxlength="4" class="form-control" id="planYear" placeholder="Year">
        </div>
      </div>
        
      <div class="form-group">
        <label for="planType" class="col-sm-2 control-label">Plan Type</label>
        <div class="col-sm-3">
            <select id="planType" name="type">
                <option value="all">All</option>
                <option value="ma">ma</option>
                <option value="pdp">pdp</option>
                <option value="ms">ms</option>
            </select>
        </div>
      </div>
        
      <div class="form-group">
        <label for="zipcode" class="col-sm-2 control-label">Zipcode</label>
        <div class="col-sm-3">
            <input type="number" required value="" name="zipcode" maxlength="5" class="form-control" id="availableIn" placeholder="Zipcode">
        </div>
      </div>
        
      <div class="form-group">
        <label for="orderBy" class="col-sm-2 control-label">Order by</label>
        <div class="col-sm-3">
            <select id="orderBy" name="orderBy">
                <option value="name">Name</option>
                <option value="rating">rating</option>
            </select>
            
            <select id="orderDir" name="orderDir">
                <option value="asc">Asc</option>
                <option value="desc">Desc</option>
            </select>
        </div>
      </div>
        
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary">Search</button>
        </div>
      </div>

    </form>
</div>

<div class="row results-sec">
    
    <table class="table table-striped hide-monthly-drug" id="tblPlans">
        <thead>
            <tr>
                <th>Plan name</th>
                <th>Year</th>
                <th>Type</th>
                <th>Drug Coverage</th>
                <th class="hide-cel">Monthly Drug Premium</th>
                <th>Monthly Cost</th>
                <th>Rating</th>
                <th>Carrier</th>
                <th>Zipcode</th>
            </tr>
        </thead>
        <tbody id="plans_list">
            
        </tbody>
    </table>
</div>


<script type="text/javascript" src="{{ URL::asset('js/cignium.js') }}"></script>

@stop