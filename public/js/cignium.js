

$( "#searchForm" ).on( "submit", function( event ) {
  event.preventDefault();
  var queryData = $( this ).serialize();
  
  $.ajax({
    dataType: "json",
    url: "api/insurance",
    data: queryData,
    success: function( response ) {
          $('#plans_list').empty()
          var hasAnyPlanDrugCoverage = false;

          for (var i=0; i < response.data.plans.length; i++) {
              var rec = response.data.plans[i];

              $('#plans_list')
                  .append($('<tr>')
                    .append($('<td>').text(rec.name))
                    .append($('<td>').text(rec.year))
                    .append($('<td>').text(rec.type))
                    .append($('<td>').text((rec.drugCoverage) ? 'Yes' : 'No'))
                    .append($('<td class="hide-cel">').text(rec.monthlyDrugPremium))
                    .append($('<td>').text(rec.monthlyCost))
                    .append($('<td>').text(rec.rating))
                    .append($('<td>').text(rec.carrier))
                    .append($('<td>').text(rec.zipcode))

              );
      
              if (rec.drugCoverage) {
                  hasAnyPlanDrugCoverage = true;
              }
              
          }
          
          if (hasAnyPlanDrugCoverage){
              $('#tblPlans').removeClass('hide-monthly-drug');
          } else {
              $('#tblPlans').addClass('hide-monthly-drug');
          }
          
      }
    });
  
});



